
Programs were run on Xcode, or compiled and run on iOS Mac Terminal using “g++ -std=c++11 FILENAME.cpp -o demo”

4.1
$ g++ -std=c++11 hw4_1.cpp -o demo
$ ./demo

The code is incorrect (cout is not guaranteed to be flushed). If “cout << h()” can’t complete properly (for example, if h() calls f(), the program will be stuck in “cout << h()” and won’t ever call “cout.flush()”, or if the function of “<<” is changed from g(), or other possible errors). To fix the problem, I use try-catch method, and call “cout.flush” in catch, so as to ensure that it will be called no matter “cout << h()” is executed properly or not.

$ g++ -std=c++11 hw4_1_ec.cpp -o demo
$ ./demo
extra credit:
It’s possible to stop it. To stop the endless recursion, we can use try outside the function, detecting the recursion. If it happens, the try-catch method will help stop the recursion. Or, a easy and crude method is to detect the recursion in g(), if the recursion happens, stop the program in g(); 


4.2
$ g++ -std=c++11 hw4_2.cpp -o demo
$ ./demo

I tried dynamic_cast animal->dog, dog->dog, cat->dog using both reference and pointer. Bad casts of reference are caught animal->dog and cat->dog, while dog->dog works fine.



4.3 

RAII is easier and safer. Bjarne Stroutstrup mentioned, ”In realistic systems, there are far more resource acquisitions than kinds of resources, so the "resource acquisition is initialization" technique leads to less code than use of a "finally" construct."

C++ uses RAII, which means that objects that are defined at a certain scope are always going to be destroyed once that scope is exited. Destructors of local variables are always called however you leave scope.
If the language doesn't have deterministic destructors, we will need to write the finally block, otherwise there will be memory leak. But we don’t need to bother worrying that in C++. Also, if we have several resources that need to be acquired then freed in an exception-safe manner, it may end up with several layers deep of try / finally blocks. In addition, using try/finally, the last operation may end up being a decent bit away from the first operation. Try/finally takes much more resources than using RAII. So, RAII works much better than finally.
Example:

class File_handle {
    FILE* p;
public:
    File_handle(const char* n, const char* a)
    { p = fopen(n,a); if (p==0) throw Open_error(errno); }
    File_handle(FILE* pp)
    { p = pp; if (p==0) throw Open_error(errno); }
    
    ~File_handle() { fclose(p); }
    
    operator FILE*() { return p; }
    
    // ...
};

void f(const char* fn)
{
    File_handle f(fn,"rw");	// open fn for reading and writing
    // use file through f
}
In a system, we need a "resource handle" class for each resource. 
However, we don't have to have an "finally" clause for each acquisition of a resource. 


4.4
$ g++ -std=c++11 hw4_4.cpp -o demo
$ ./demo

Instead of using new and delete, I modified the code with smart pointers. I used shared_ptr for most of the cases. 
