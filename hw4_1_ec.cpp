//
//  main.cpp
//  hw4_1_ec
//
//  Created by Cynthia on 02/02/2017.
//  Copyright © 2017 Cynthia. All rights reserved.
//

#include <iostream>

using namespace std;
void g();
int f();

// method 1:
void g() {
    static int locked = 0;
    if (locked++){
        throw locked;
    };
}

int f() {
    cout << "Some text";
    g(); // g and h are functions whose
    cout << f(); // definitions are unknown
    cout.flush();
    return 0;
}
int main() {
    try {
       f();
    }
    catch(int locked) {
        cout << "Recursion detected." << endl;
    }

    return 0;
}

// method 2:
//void g() {
//    static int locked = 0;
//    if (locked++){
//        cout << "Recursion detected.";
//        exit(0);
//    };
//}
//
//int f() {
//    cout << "Some text";
//    g(); // g and h are functions whose
//    cout << f(); // definitions are unknown
//    cout.flush();
//    return 0;
//}
//
//int main() {
//    
//    f();
//    
//    return 0;
//}
