//
//  main.cpp
//  hw4_2
//
//  Created by Cynthia on 02/02/2017.
//  Copyright © 2017 Cynthia. All rights reserved.
//

#include <iostream>

using namespace std;

class Animal {
public:
    Animal(){};
    virtual ~Animal(){}
};

class Dog : public Animal{
};

class Cat : public Animal {
};


int main(int argc, const char * argv[]) {
    // insert code here...
    Animal *animal = new Animal;
    Animal *dog = new Dog;
    Animal *cat = new Cat;
    
    // using reference
    try {
        Dog& dog1 = dynamic_cast<Dog&>(*animal);
    }
    catch(bad_cast exp) {
        cout << "Bad cast of reference from animal to dog." << endl;
    }
    try {
        Dog& dog2 = dynamic_cast<Dog&>(*dog);
    }
    catch(bad_cast exp) {
        cout << "Bad cast of reference from dog to dog." << endl;
    }
    try {
        Dog& dog3 = dynamic_cast<Dog&>(*cat);
    }
    catch(bad_cast exp) {
        cout << "Bad cast of reference from cat to dog." << endl;
    }
    
    //Using pointer
    try {
        Dog* dog4 = dynamic_cast<Dog*>(animal);
        if( dog4 == NULL) {
            cout<<"NULL Pointer(4)\n";
        }
    }
    catch(std::bad_cast exp) {
        cout<<"Caught bad cast of pointer from animal to dog\n";
    }
    
    try {
        Dog* dog5 = dynamic_cast<Dog*>(dog);
        if( dog5 == NULL) {
            cout<<"NULL Pointer(5)\n";
        }
    }
    catch(std::bad_cast exp) {
        cout<<"Caught bad cast of pointer from animal to dog\n";
    }

    
    try {
        Dog* dog6 = dynamic_cast<Dog*>(cat);
        
        if( dog6 == NULL) {
            cout<<"NULL Pointer(6)\n";
        }
    }
    catch(std::bad_cast exp) {
        cout<<"Caught bad cast of pointer from animal to dog\n";
    }
    
    
    return 0;
    
}
