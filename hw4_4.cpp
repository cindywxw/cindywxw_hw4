//
//  main.cpp
//  hw4_4
//
//  Created by Cynthia on 02/02/2017.
//  Copyright © 2017 Cynthia. All rights reserved.
//

//  modification on http://www.cprogramming.com/tutorial/lesson18.html
// original statements are commented out, modified version follows right after
#include <iostream>

using namespace std;

struct node
{
    int key_value;
//    node *left;
//    node *right;
    shared_ptr<node> left;
    shared_ptr<node> right;
   
};
class btree
{
public:
    btree();
    ~btree();
    
    void insert(int key);
//    node *search(int key);
    shared_ptr<node> search(int key);
    void destroy_tree();
    
private:
//    void destroy_tree(node *leaf);
//    void insert(int key, node *leaf);
//    node *search(int key, node *leaf);
//    
//    node *root;
    void destroy_tree(shared_ptr<node> leaf);
    void insert(int key, shared_ptr<node> leaf);
    shared_ptr<node> search(int key, shared_ptr<node> leaf);
    shared_ptr<node> root;
};

btree::btree()
{
    root=NULL;
}

//void btree::destroy_tree(node *leaf)
void btree::destroy_tree(shared_ptr<node> leaf)
{
    if(leaf!=NULL)
    {
        destroy_tree(leaf->left);
        destroy_tree(leaf->right);
//        delete leaf;
        leaf.reset();
    }
}

void btree::insert(int key, shared_ptr<node> leaf)
{
//    node *tmp = leaf.get();
    
    if(key < leaf->key_value)
    {
        if(leaf->left!=NULL)
//            insert(key, leaf->left);
            insert(key, move(leaf->left));
        else
        {
//            leaf->left=new node;
            leaf->left = make_shared<node>();
            leaf->left->key_value=key;
            leaf->left->left=NULL;
            leaf->left->right=NULL;
        }
    }
    else if(key>=leaf->key_value)
    {
        if(leaf->right!=NULL)
//            insert(key, leaf->right);
            insert(key, leaf->right);
        else
        {
//            leaf->right=new node;
            leaf->right = make_shared<node>();
            leaf->right->key_value=key;
            leaf->right->left=NULL;
            leaf->right->right=NULL;
            
        }
    }
}

//node *btree::search(int key, node *leaf)
shared_ptr<node> btree::search(int key, shared_ptr<node> leaf)
{
    if(leaf!=NULL)
    {
        if(key==leaf->key_value)
            return leaf;
        if(key<leaf->key_value)
            return search(key, leaf->left);
        else
            return search(key, leaf->right);
    }
    else return NULL;
}

void btree::insert(int key)
{
    if(root!=nullptr)
        insert(key, root);
    else
    {
//        root=new node;
        auto root = make_shared<node>();
        root->key_value=key;
        root->left=nullptr;
        root->right=nullptr;
    }
}

//node *btree::search(int key)
shared_ptr<node> btree::search(int key)

{
    return search(key, root);
}

void btree::destroy_tree()
{
    destroy_tree(root);
}







