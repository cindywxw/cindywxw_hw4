//
//  main.cpp
//  hw4_1
//
//  Created by Cynthia on 02/02/2017.
//  Copyright © 2017 Cynthia. All rights reserved.
//

#include <iostream>
#include <exception>

using namespace std;
void g();
int h();
int f();
class myexception: public exception
{
    virtual const char* what() const throw()
    {
        return "My exception happened";
    }
} myex;

void g() {
    static int lock = 0;
    if (lock++) {
        throw lock;
    }
    return;
}
int h() {

//    f(); // case 1: recursion.
    
    throw myex; // case 2: simulate any other possible error that may happen.
    
    return 0;
}

int f() {
    cout << "Some text";
    g(); // g and h are functions whose
    cout << h(); // definitions are unknown
//    printf("flushed");      // used this line to check if the next "cout.flush()" is executed. If it's not printed, "cout.flush()" is not executed either.
    cout.flush();
    return 0;
}

int main() {
//    f();      // if we simply call f(), errors or recursion may happen which results in cout.flush() not executed.
    
    // use try-catch to fix the code.
    try {
        f();
    }
    catch (int lock){
        cout<<"Recursion detected.";
        cout.flush();
    }
    catch(exception & e) {
        cout<<"Exception detected.";
        cout.flush();
    }
    
    return 0;
}
